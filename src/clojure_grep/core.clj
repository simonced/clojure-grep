(ns clojure-grep.core
  (:gen-class)
  (:import (javax.swing JFrame))
  (:require [clojure-grep.gui :as gui]
            [clojure.string :as str]
            [clojure.java.shell :as shell]))

;;; main settings / constants
(def version "0.0.5")

(defn parse-grep-results
  "Splits search results into a structure for JTable.
  This function parses one line, intended to be used with map.
  @return {:file ... :file ... :sample ...}"
  [results_]
  (let [parts (str/split results_ #":")
        ; FIXME use matching groups instead, now, this only works for Windows!
        file (str/join ":" (take 2 parts))
        line (nth parts 2)
        sample (str/trim (str/join "" (drop 3 parts)))]
    {:file file :line (read-string line) :sample sample}))

;;; search action
(defn search
  "Start search
  Params are text values"
  [where_ what_ table_]
  (let [{exit :exit, out :out, err :err} (shell/sh "grep" "-nHr" what_ where_)]
    ;(prn "where:" where_ "What:" what_ "Table:" table_)
    ;(prn exit out err)
    (if (= 0 exit)
      ; in case of success
      (do
        (->> out
             str/split-lines
             (map parse-grep-results)
             (gui/update-table table_)))
      ; in case of error
      (do
        ;; (prn "ERR:", err)
        (gui/update-status-bar "Search error...")))))

(def default-where
  "Where we search by default.
  For now, we search in this project directory.
  TODO save last search location in a preferences files."
  (System/getProperty "user.dir"))

;;; Basic window container
(def window (gui/build-window
              (clojure.string/join ["Clojure Grep v." version])
              default-where
              search))

;;; Program entry point
(defn -main
  "Starts the GUI."
  []
  (doto window
    (.setLocationRelativeTo nil)
    (.setVisible true)
    ;; default action
    (.setDefaultCloseOperation JFrame/EXIT_ON_CLOSE))
  (gui/add-tab))
