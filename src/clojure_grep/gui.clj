(ns clojure-grep.gui
	(:import (javax.swing JLabel JTabbedPane JFrame JPanel JButton JScrollPane JTable BoxLayout JTextField)
          (java.awt FlowLayout BorderLayout)
          (java.awt.event ActionListener)
          (javax.swing.table DefaultTableModel JTableHeader)
          (javax.swing.border EmptyBorder)))

(def gui-frame (JFrame.))

;;; status bar so we can reference it
(def status-bar (JLabel. "Status: TODO"))

;;; default padding (empty border)
(def padding-default (EmptyBorder. 10 10 10 10))

;;; main container (tabs container)
(def tabs-container (JTabbedPane.))

;;; search action provided by core
(def search-action nil)

;;; default search location
(def default-where nil)

;;; list of tabs and the important elements like inputs and table (for results)
(def tabs (atom []))
; content format
; {:what "the regex" :where "the where input" :table "The results table"}

(defn update-status-bar
  "Change the text in the status bar"
  [newText_]
  (.setText status-bar newText_))


;;; Access

(defn current-form
  "Return the current tab container from the tabs atom"
  []
  (nth @tabs (.getSelectedIndex tabs-container)))

(defn get-form-values
  "Retrieve form values
  Param is a structure returned from @tabs (one entry)"
  [tab_]
  {:where (-> tab_ :where .getText)
   :what  (-> tab_ :what .getText)})

;;; actions

(def btn-search-action-proxy
  (proxy [ActionListener] []
    (actionPerformed [event]
      (let [tab (current-form)
            {what :what where :where} (get-form-values tab)]
        (search-action where what (:table tab))))))

;;; creation

(defn make-tab-form
  "Creates a form section that goes in top of the Tab content"
  []
  ; WIP
  (let [container (JPanel. (BorderLayout.))
        left (JPanel.)
        right (JPanel.)
        input-what (JTextField.)
        input-where (JTextField. default-where)
        button (JButton. "Search")]
		; left part
    (.addActionListener button btn-search-action-proxy)
    (doto left
      (.setLayout (BoxLayout. left BoxLayout/Y_AXIS))
      (.add (JLabel. "Where? (last / not required)"))
      (.add input-where)
      (.add (JLabel. "What? (regexp format)"))
      (.add input-what)
      )
		; right part
    (.add right button)
		; assembly
    (doto container
      (.setBorder padding-default)
      (.add left BorderLayout/CENTER)
      (.add right BorderLayout/LINE_END)
      )
    {:container container :what input-what :where input-where :btn button}
    )
  )

(defn- make-table-model
  "Creates default table model"
  []
  (let [model (DefaultTableModel.)]
    (doto model
      (.addColumn "file")
      (.addColumn "line")
      (.addColumn "sample")
      )
    model)
  )

(defn make-tab-listing
  "Creates a table to display search results.
  Its container is a JScrollPane"
  []
  (let [table (JTable.)
        table-model (make-table-model)
        headers (JTableHeader.)]
    (doto table
      (.setModel table-model)
      )
		;(.setTableHeader table headers)
    {:container (JScrollPane. table) :table table})
  )

(defn add-tab
  "Add a new tab"
  []
  (update-status-bar "WIP")                                  ; temporary
  (let [new-tab (JPanel. (BorderLayout.))
        inputs (make-tab-form)
        output (make-tab-listing)]
    (doto new-tab
      (.add (:container inputs) BorderLayout/NORTH)
      (.add (:container output) BorderLayout/CENTER))
    (.addTab tabs-container "New Search" new-tab)
    ; focus new tab
    (.setSelectedIndex tabs-container (- (.getTabCount tabs-container) 1))
    ; update status about it
    (update-status-bar "New tab created and focused.")
    ; TODO add action to button in (:btn inputs)
    ; add elements to tabs atom
    (swap! tabs conj {:what (:what inputs) :where (:where inputs) :table (:table output)})
    )
  )

(defn close-tab
  "Close the active search tab"
  []
  (prn "TODO"))

(def btn-addtab-action-proxy
	(proxy [ActionListener] []
		(actionPerformed [event] (add-tab))))

(def btn-closetab-action-proxy
    (proxy [ActionListener] []
      (actionPerformed [event] (close-tab))))

(defn make-top-bar
  "Builds what goes at the top of the window"
  []
  (let [bar-container (JPanel. (FlowLayout. FlowLayout/RIGHT))
        button-newsearch (JButton. "New Search")
        button-closesearch (JButton. "Close Search")]
    (.addActionListener button-newsearch btn-addtab-action-proxy)
    (.add bar-container button-newsearch)
    (.addActionListener button-closesearch btn-closetab-action-proxy)
    (.add bar-container button-closesearch)
    bar-container))

(defn make-tabs-container
  "Builds the tabs container that goes in the center"
  []
  tabs-container)

(defn make-status-bar
  "Builds the status bar"
  []
  (let [status-container (JPanel. (FlowLayout. FlowLayout/LEFT))]
    (.add status-container status-bar)
    status-container))

;;; table data
(defn update-table
  "Update table content"
  [table_ data_]
  (let [table-model (make-table-model)
        fm (.getFontMetrics table_ (.getFont table_))
        max-filename-size (apply max (map #(.stringWidth fm (:file %)) data_))
        max-line-size (apply max (map #(.stringWidth fm (str (:line %))) data_))
        add-column-padding #(+ % (.stringWidth fm "aaa"))]
    ;; (prn max-filename-size)
    (dotimes [n (count data_)]
      (.addRow table-model (to-array [
                                      (:file (nth data_ n))
                                      (:line (nth data_ n))
                                      (:sample (nth data_ n))])))
    (.setModel table_ table-model)
    ;; update table columns width
    (-> (.getColumnModel table_)
        (.getColumn 0)
        (doto 
            (.setMaxWidth (add-column-padding max-filename-size))
          (.setMinWidth (add-column-padding max-filename-size))))
    (-> (.getColumnModel table_)
        (.getColumn 1)
        (doto
            (.setMaxWidth (add-column-padding max-line-size))
          (.setMinWidth (add-column-padding max-line-size))))
    ))

;;; Interface

(defn build-window
	"Create the basic window.
	search-action_ is a function for the search buttons"
	[title_ defaul-where_ search-action_]
  (def search-action search-action_)
  (def default-where defaul-where_)
	(doto gui-frame
    (.setTitle title_)
		(.setSize 800 600)
		(.add (make-top-bar) BorderLayout/PAGE_START)
		(.add (make-tabs-container) BorderLayout/CENTER)
		(.add (make-status-bar) BorderLayout/PAGE_END)
		;; pack it
		; (.pack gui-frame)
    )
	;; and return
	gui-frame)
